from django.contrib import admin
from .models import Robot, RobotState, RobotActions

admin.site.register(Robot)
admin.site.register(RobotState)
admin.site.register(RobotActions)