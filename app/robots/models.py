from django.db import models


class Robot(models.Model):
    name=models.CharField(max_length=30)

    def __str__(self):
        return self.name


class RobotState(models.Model):
    name=models.CharField(max_length=30)
    robot=models.ForeignKey(Robot, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class RobotActions(models.Model):
    name=models.CharField(max_length=30)
    state_start=models.ForeignKey(RobotState, related_name='action_state_start', on_delete=models.CASCADE)
    state_end=models.ForeignKey(RobotState, related_name='action_state_end', on_delete=models.CASCADE)
    time=models.IntegerField()

    def __str__(self):
        return self.name