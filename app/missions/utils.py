from django.http import JsonResponse
from robots.models import RobotState, RobotActions

def dijkstra(graph, initial, goal):
    visited = {initial: 0}
    path = {}
    nodes = set(graph.keys())

    while nodes:
        min_node = None
        for node in nodes:
            if node in visited:
                if min_node is None:
                    min_node = node
                elif visited[node] < visited[min_node]:
                    min_node = node
        if min_node is None:
            break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in graph[min_node]:
            weight = current_weight + graph[min_node][edge]
            if edge not in visited or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node

    full_path = []
    current = goal
    while current is not None:
        full_path.append(current)
        current = path.get(current)
    return full_path[::-1], visited[full_path[-1]]

def create_graph(actions):
    graph = {}
    for action in actions:
        start = action.state_start.name
        end = action.state_end.name
        if start not in graph:
            graph[start] = {}
        graph[start][end] = action.time
    return graph

