from django.db import models

from django.db import models
from robots.models import RobotState

class Mission(models.Model):
    name=models.CharField(max_length=30)
    init=models.ForeignKey(RobotState, related_name='mission_init_start', on_delete=models.CASCADE)
    goal=models.ForeignKey(RobotState, related_name='mission_goal_start', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
