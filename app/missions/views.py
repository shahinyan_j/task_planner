from django.shortcuts import render
from robots.models import Robot, RobotState
from django.http import JsonResponse
from robots.models import RobotState, RobotActions
from .utils import dijkstra, create_graph

def index(request):
    robots=Robot.objects.all()
    states=RobotState.objects.all()
    context = {
        'robots': robots,
        'states': states
    }
    return render(request, "missions/index.html", context)


def plan_mission(request, init_state_name, goal_state_name):
    initial_state = RobotState.objects.get(name=init_state_name)
    goal_state = RobotState.objects.get(name=goal_state_name)

    actions = RobotActions.objects.all()

    graph = create_graph(actions)

    path, total_time = dijkstra(graph, initial_state.name, goal_state.name)

    response_data = {
        "actions": path,
        "time": total_time
    }

    return JsonResponse(response_data)
    # return render(request, "missions/index.html", response_data)


