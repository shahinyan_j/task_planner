from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("plan_mission", views.plan_mission, name="plan_mission")
]